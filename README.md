# TESTING AUTOMÁTICO #

### Conocimientos previos ###

* Verbos Http (POST , PUT , GET , DELETE )

### Introduccion ###

* ¿ Que es Cucumber(BDD) ?

Cucumber es una herramienta utilizada para implementar metodologías como BDD (Behaviour Driven Development) las cuales permiten ejecutar descripciones funcionales escritas en texto plano como pruebas de software automatizadas.

Estas descripciones funcionales, se escriben en un lenguaje específico y legible llamado "Gherkin”, el cual sirve como documentación al desarrollo y para las pruebas automatizadas.



* ¿ Que es Gherkins ?

Gherkin es un lenguaje creado para ser comprensible y fácil de leer, tanto para la parte técnica como a la de negocio y con el que vamos a describir las funcionalidades, dependiendo del comportamiento del software, sin entrar en su implementación.
 
* ¿Cuáles son las sentencias que debemos usar en Gherkin?

Feature: Indica el nombre de la funcionalidad que vamos a probar de una forma clara y expícita. A partir de esta funcionalidad, comenzaremos a construir nuestros escenarios de prueba.

Scenario: Describe cada escenario que vamos a probar.

Given: Esta sentencia provee el contexto en el cual el escenario se va a ejecutar dentro del test. También podemos describir los pre-requisitos en él y toda la información necesaria para comenzar el test.

When: Especifica el conjunto de acciones que lanzan el test dentro de un escenario, definiendo así la interacción del usuario que acciona la funcionalidad que deseamos probar.

Then: Especifica el resultado esperado en el test.

* ¿ Que són y cómo definimos los escenarios ?

Un escenario describe un ejemplo de comportamiento del sistema, los escenarios son condiciones de aceptación de una historia de usuario.

Los escenarios se pueden agrupar dentro de funcionalidades, cada una de ellas definida en un fichero .feature que define la funcionalidad a probar.

* ¿ Que son los tags ?

Un tag de cucumber es un identificador del escenario que utiliza la anotación @

* ¿ Cómo funciona Cucumber ?

1 – Cucumber cogería primero el caso de prueba definido en una carpeta llamada features, un archivo con extensión .features que dice Dado un contexto inicial.


Utilizaremos el siguiente ejemplo de escenario ubicado en `/src/test/resources/features/users/obtenerToken.feature`

```
Feature: Users
  @mifid-users=01
  Scenario: Obtener el token de usuario
  Given Realizaremos una petición en empresas para obtener el token del usuario
```

2 – A continuación, busca en un paquete llamado Steps

Ubicado en `/src/test/java/starter/users/steps/UserTokenStepsDefinition.java`

Podemos comprobar que el contenido que hay utilizando la anotación Given es el mismo contenido que hay en el fichero feature.

3 – Ejecuta el código que tenga dentro

4 – Repite el mismo proceso para el resto de los casos de prueba

### Lanzando el proyecto ###

Cuando tengamos clonado el repositorio vamos a lanzar el proyecto maven existente añadiendo el parámetro perfil (Es obligatorio que el valor sea int , pre , local).

En el archivo `/src/test/resources/serenity.conf` podemos ver los entornos que tenemos definidos

Para lanzar los test de integración ejecutaremos desde la consola el siguiente comando maven.

* Multi-Entorno

```
mvn clean verify -Denvironment=int
```

* Tags


```
mvn clean verify -Denvironment=int -Dcucumber.options="--tags @mifid-users=01"
```

* Multi-Tags

### Estructura del proyecto ###

Vamos a intentar entender como está el proyecto estructurado , una vez hayamos realizado un test veremos que prácticamente son todos iguales.

Crearmos cuatro paquetes (Uno por API) que vayamos a probar (Las colecciones son las que se encuentran en postman)

`/src/test/java/starter/users/constants/`: En el paquete de constantes tendremos todas los valores estáticos que tengamos que utilizar tanto en el paquete de steps y utils , de esta forma estarán centralizadas.

`/src/test/java/starter/users/steps`: En el paquete de steps lo importante es entender que se traducne a java los pasos que hemos definidos en los feature con las anotaciones Given , When , And , y Then .

`/src/test/java/starter/users/utils` : El paquete de utils contendrá alguna operación necesaria que necesitemos para la prueba en concreto.

`/src/test/java/starter/users/model`: En el paquete de model se encontrarán los POJO que necesitaremos para cuando vayamos a realizar una peticion POST.

### Jenkins ###

* Que es un Pipeline


Un pipeline es una nueva forma de trabajar en el mundo devops en la integración continua. Utilizando pipeline y Jenkins podemos definir el ciclo de vida completo de una aplicación (descargar código, compilar, test, desplegar, etc.) mediante código.

De esta forma, resulta mucho más sencillo replicar los diferentes pasos con distintas aplicaciones y gestionar mejor los cambios en cada paso.


* Configurar el Pipeline


```
pipeline {
    agent any
    tools { 
        maven 'maven' 
        jdk 'jdk8' 
    }
    stages {
        stage ('Initialize') {
            steps {
                bat '''
                    echo "PATH = ${PATH}"
                    echo "M2_HOME = ${M2_HOME}"
                ''' 
            }
        }
        
        stage('Checkout') {
           steps {
               git 'https://bitbucket.org/jnfz92/02-language'
           }
        }
        
        stage('Build Jar'){
            steps {
                bat 'mvn package -Dmaven.test.skip=true'
            }
        }
        
    }
}
```



### Rest Assured ###

* POST
* PUT
* GET
* DELETE
* Reporte de logs del Serenity
* Como convertir de JSON a Pojo

```
http://www.jsonschema2pojo.org/
```

* Reporte de logs del Serenity

### Creción de una prueba completa desde 0 ###

#### Creación del fichero feature

En el fichero .feature es donde vamos a definir los diferentes criterios de aceptación (Estos criterios de aceptación están en los excel que nos adjuntan en el jira).

Vamos a ir a la siguiente ubicación [/src/test/resources/features](https://bitbucket.org/juanfranciscofernandez/cucumber-serenity/src/master/src/test/resources/features)  y crearemos un fichero con la terminación .feature

Miraremos el ejemplo de  [oficinas y cajeros](https://bitbucket.org/juanfranciscofernandez/cucumber-serenity/src/master/src/test/resources/features/locations/locations.feature) .

Como podemos observar es un ejemplo bastante sencillo ya que al final únicamente comprobamos la respuesta recibida.

#### Creando los pasos a definir con Java

Anteriormente hemos estudiado el fichero feature de [oficinas y cajeros](https://bitbucket.org/juanfranciscofernandez/cucumber-serenity/src/master/src/test/resources/features/locations/locations.feature) pero ahora vamos a visualizar la [clase java](https://bitbucket.org/juanfranciscofernandez/cucumber-serenity/src/master/src/test/java/starter/locations/steps/LocationsStepsDefinition.java) dónde vamos a realizar todas las definiciones.

Creo que se puede comprender que las anotaciones Given , When , Then e And que escribimos en el fichero feature están vinculadas con un fichero Step.